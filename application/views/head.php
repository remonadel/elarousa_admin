<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <title>ElArousa Admin</title>
        <!-- Stylesheets -->
        <link rel="stylesheet" type="text/css" href="<?= ASSETS; ?>css/font-awesome.css" /><!-- Font Awesome -->
        <link rel="stylesheet" type="text/css" href="<?= ASSETS; ?>css/bootstrap.css" /><!-- Bootstrap -->
        <link rel="stylesheet" type="text/css" href="<?= ASSETS; ?>css/style.css" /><!-- Style -->
        <link rel="stylesheet" type="text/css" href="<?= ASSETS; ?>css/responsive.css" /><!-- Responsive -->
        <link rel="stylesheet" type="text/css" href="<?= ASSETS; ?>css/jquery-ui.min.css" />
        <link rel="stylesheet" type="text/css" href="<?= ASSETS; ?>css/jquery.datetimepicker.css" />
        <link rel="stylesheet" type="text/css" href="<?= ASSETS; ?>css/jquery.timepicker.css" />
        <link rel="stylesheet" type="text/css" href="<?= ASSETS; ?>css/jquery.Jcrop.min.css" />
        <link rel="stylesheet" type="text/css" href="<?= ASSETS; ?>css/dropzone.css" />
        <link rel="stylesheet" type="text/css" href="<?= ASSETS; ?>css/sweetalert.css" />
        <link rel="stylesheet" type="text/css" href="<?= ASSETS; ?>css/google.css" />
        <link rel="stylesheet" type="text/css" href="http://select2.github.io/select2/select2-3.3.2/select2.css"/>
        <link rel="stylesheet" type="text/css" href="http://t0m.github.io/select2-bootstrap-css/select2-bootstrap.css"/>
        <!-- JS files -->
        <script src="<?= ASSETS; ?>js/jquery-1.11.2.min.js"></script>
        <script src="<?= ASSETS; ?>js/jquery-ui.min.js"></script>
        <script src="<?= ASSETS; ?>js/jquery.datetimepicker.js"></script>
        <script src="<?= ASSETS; ?>js/jquery.timepicker.js"></script>
        <script src="<?= ASSETS; ?>js/jquery.Jcrop.min.js"></script>
        <script src="<?= ASSETS; ?>js/template_Jcrop_script.js"></script>
        <script src="<?= ASSETS; ?>js/bootstrap.js"></script>
        <script src="<?= ASSETS; ?>js/sweetalert.min.js"></script>
        <script src="<?= ASSETS; ?>tinymce/tinymce.min.js"></script>
        <script src="<?= ASSETS; ?>js/dropzone.js"></script>
        <script src="<?= ASSETS; ?>js/modernizr.js"></script>
        <script src="<?= ASSETS; ?>js/enscroll.js"></script>
        <script src="<?= ASSETS; ?>js/script.js"></script>
       <script src="http://select2.github.io/select2/select2-3.3.2/select2.js"></script>
    </head>
	