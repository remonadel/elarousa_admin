<?php $this->load->view("head.php"); ?>
<body>
    <div class="main">
        <header class="header">
            <div class="dropdown profile">
                <a title="">
                    <i class="caret" style="margin-left: 10px;margin-top: 16px;float: right;"></i>
                    <p class="p-name-pro"><?= $this->session->userdata("name"); ?></p>
                    <?php $user_pic = $this->session->userdata("picture"); ?>
                    <?php if ( ! empty($user_pic)): ?> 
                        <img src="<?= USER_PHOTOS; ?><?= $this->session->userdata("picture"); ?>" alt="<?= $this->session->userdata("name"); ?>" />
                    <?php endif; ?> 
                </a>
                <div class="profile drop-list">
                    <ul>
                        <li><a href="<?= site_url(); ?>change_password" title=""><i class="fa fa-edit"></i>تغيير كلمة السر</a></li>
                        <li><a href="<?= site_url(); ?>change_picture" title=""><i class="fa fa-user"></i>تغيير الصورة</a></li>
                    </ul>
                </div>
            </div>
            <div class="logo">
                <a href="" title=""></a>
                <a href="#" class="toggle-menu"><i class="fa fa-bars"></i></a>
            </div>
            <div class="custom-dropdowns">
                <div class="message-list dropdown">

                    <!--<div class="message drop-list">
                        <span style="direction: ltr;"><span id="unseen-msgs-text">No</span> unseen messages</span>
                        <ul>
                            <li>
                                <a href="#" title=""><span><img src="<?= ASSETS; ?>images/resource/sender1.jpg" alt="" /></span><i><?= $this->session->userdata("name"); ?></i>Hi! How are you?...<h6>2 min ago..</h6><p class="status blue">New</p></a>
                            </li>
                            <li>
                                <a href="#" title=""><span><img src="<?= ASSETS; ?>images/resource/sender2.jpg" alt="" /></span><i>Jonathan</i>Hi! How are you?...<h6>2 min ago..</h6><p class="status red">Unsent</p></a>
                            </li>
                            <li>
                                <a href="#" title=""><span><img src="<?= ASSETS; ?>images/resource/sender3.jpg" alt="" /></span><i>Barada knol</i>Hi! How are you?...<h6>2 min ago..</h6><p class="status green">Reply</p></a>
                            </li>
                            <li>
                                <a href="#" title=""><span><img src="<?= ASSETS; ?>images/resource/sender4.jpg" alt="" /></span><i>Samtha Gee</i>Hi! How are you?...<h6>2 min ago..</h6><p class="status">New</p></a>
                            </li>
                        </ul>
                        <a href="<?= site_url(); ?>messages" title="">See All Messages</a>
                    </div>-->
            </div>
                <!--<div class="notification-list dropdown">
                    <a title=""><span class="green">0</span><i class="fa fa-bell-o"></i></a>
                    <div class="notification drop-list">
                        <span>No new notifications</span>
                        <ul>
                            <li>
                                <a href="#" title=""><span><i class="fa fa-bug red"></i></span>Server 3 is Overloaded Please Check... <h6>2 min ago..</h6></a>
                            </li>
                            <li>
                                <a href="#" title=""><span><img src="<?= ASSETS; ?>images/resource/sender2.jpg" alt="" /></span><i>MD Daisal</i>New User Registered<h6>4 min ago..</h6><p class="status red">Urgent</p></a>
                            </li>
                            <li>
                                <a href="#" title=""><span><i class="fa fa-bullhorn green"></i></span>Envato Has change the policies<h6>7 min ago..</h6></a>
                            </li>
                        </ul>
                        <a href="#" title="">See All Notifications</a>
                    </div>
                </div>
                <div class="activity-list dropdown">
                    <a title=""><span class="red">0</span><i class="fa fa-clock-o"></i></a>
                    <div class="activity drop-list">
                        <span>Recent Activity</span>
                        <ul>
                            <li>
                                <a href="#" title=""><span><img src="<?= ASSETS; ?>images/resource/sender2.jpg" alt="" /></span><i>Jona Than</i>Uploading new files<h6>2 min ago..</h6><p class="status green">Online</p></a>
                                <div class="progress">
                                    <div style="width: 60%;" aria-valuemax="100" aria-valuemin="0" aria-valuenow="60" role="progressbar" class="progress-bar blue">
                                        60%
                                    </div>
                                </div>
                            </li>
                            <li>
                                <a href="#" title=""><span><img src="<?= ASSETS; ?>images/resource/sender1.jpg" alt="" /></span><i>Bela Nisaa</i>Downloading new Documents<h6>2 min ago..</h6></a>
                                <div class="progress">
                                    <div style="width: 34%;" aria-valuemax="100" aria-valuemin="0" aria-valuenow="34" role="progressbar" class="progress-bar red">
                                        34%
                                    </div>
                                </div>
                            </li>
                        </ul>
                        <a href="#" title="">See All Activity</a>
                    </div>
                </div>
            </div>	
            <!--<a title="" class="slide-panel-btn"><i class="fa fa-gear fa-spin"></i></a>-->
        </header>
        
        <div class="page-container menu-left">
            <aside class="sidebar">
                <div class="profile-stats">
                    <div class="mini-profile">
                        <?php $user_pic = $this->session->userdata("picture"); ?>
                        <?php if ( ! empty($user_pic)): ?> 
                            <span><img src="<?= USER_PHOTOS; ?><?= $this->session->userdata("picture"); ?>" alt="<?= $this->session->userdata("name"); ?>" /></span>
                        <?php endif; ?> 
                        <h3><?= $this->session->userdata("name"); ?></h3>
                        <h6 class="status online"><i></i> متاح الان</h6>
                        <a href="<?= site_url(); ?>logout" title="Logout" class="logout red" data-toggle="tooltip" data-placement="right"><i class="fa fa-power-off"></i></a>
                    </div>
                </div>
                
                <div class="menu-sec">
                    <div id="menu-toogle" class="menus">


                        <div class="single-menu" id="cli-dr6">
                            <h2><a><i class="fa fa-ellipsis-h"></i><span>التصنيفات </span></a></h2>
                            <div class="sub-menu">
                                <ul>

                                    <li><a href="<?= site_url(); ?>categories/add">تصنيف جديد</a></li>

                                    <li><a href="<?= site_url(); ?>categories">إدارة التصنيفات</a></li>


                                </ul>
                            </div>
                        </div>
                            <div class="single-menu" id="cli-dr6">
                                <h2><a><i class="fa fa-ellipsis-h"></i><span>الخدمات</span></a></h2>
                                <div class="sub-menu">
                                    <ul>

                                            <li><a href="<?= site_url(); ?>services/add">خدمة جديدة</a></li>

                                            <li><a href="<?= site_url(); ?>services">إدارة الخدمات</a></li>


                                    </ul>
                                </div>
                            </div>


                        <div class="single-menu" id="cli-dr6">
                            <h2><a><i class="fa fa-ellipsis-h"></i><span>المدن</span></a></h2>
                            <div class="sub-menu">
                                <ul>

                                        <li><a href="<?= site_url(); ?>add_city">مدينة جديدة</a></li>

                                        <li><a href="<?= site_url(); ?>cities">إدارة المدن</a></li>


                                </ul>
                            </div>
                        </div>


                        <div class="single-menu" id="cli-dr6">
                            <h2><a><i class="fa fa-ellipsis-h"></i><span>المناطق</span></a></h2>
                            <div class="sub-menu">
                                <ul>

                                    <li><a href="<?= site_url(); ?>add_areas">منطقة جديدة</a></li>

                                    <li><a href="<?= site_url(); ?>areas">إدارة المناطق</a></li>


                                </ul>
                            </div>
                        </div>


                        <div class="single-menu" id="cli-dr12">
                            <h2><a><i class="fa fa-user"></i><span>الصفحات الثابته</span></a></h2>
                            <div class="sub-menu">
                                <ul>
                                    <li><a href="<?= site_url('pages'); ?>">عرض الكل</a></li>
                                    <li><a href="<?= site_url('pages/save'); ?>">صفحة جديدة</a></li>
                                </ul>
                            </div>
                        </div>
                        <div class="single-menu" id="cli-dr12">
                            <h2><a><i class="fa fa-user"></i><span>تفعيل الشركات</span></a></h2>
                            <div class="sub-menu">
                                <ul>
                                    <li><a href="<?= site_url('allCampanies'); ?>">عرض الكل</a></li>
                                </ul>
                            </div>
                        </div>
                    </div>
                    <p>Copyright © 2015</p>
                </div>
            </aside>
            <div class="content-sec">
                <div class="breadcrumbs">
                    <ul>
                        <li><a href="<?= site_url(); ?>" title="Home"><i class="fa fa-home"></i></a></li>
                    </ul>
                </div>
                
