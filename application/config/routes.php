<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');
/*
| -------------------------------------------------------------------------
| URI ROUTING
| -------------------------------------------------------------------------
| This file lets you re-map URI requests to specific controller functions.
|
| Typically there is a one-to-one relationship between a URL string
| and its corresponding controller class/method. The segments in a
| URL normally follow this pattern:
|
|	example.com/class/method/id/
|
| In some instances, however, you may want to remap this relationship
| so that a different class/function is called than the one
| corresponding to the URL.
|
| Please see the user guide for complete details:
|
|	http://codeigniter.com/user_guide/general/routing.html
|
| -------------------------------------------------------------------------
| RESERVED ROUTES
| -------------------------------------------------------------------------
|
| There area two reserved routes:
|
|	$route['default_controller'] = 'welcome';
|
| This route indicates which controller class should be loaded if the
| URI contains no data. In the above example, the "welcome" class
| would be loaded.
|
|	$route['404_override'] = 'errors/page_missing';
|
| This route will tell the Router what URI segments to use if those provided
| in the URL cannot be matched to a valid route.
|
*/

$route['default_controller'] = 'home';
$route['login'] = 'home/login';
$route['logout'] = 'home/logout';
$route['change_password'] = 'home/change_password';
$route['change_picture'] = 'home/change_picture';
$route['coverages/(:num)'] = "coverages";
$route['paper_versions/(:num)'] = "paper_versions";
$route['writers/(:num)'] = "writers";
$route['keywords/(:num)'] = "keywords";
$route['interactive_files/(:num)'] = "interactive_files";
$route['images/(:num)'] = "images";
$route['albums/(:num)'] = "albums";
$route['users/(:num)'] = "users";
$route['user_reports/(:num)'] = "user_reports";
$route['groups/(:num)'] = "groups";
$route['albums_featured/(:num)'] = "albums_featured/index/$1";
$route['questions/(:num)'] = "questions/index/$1";
$route['doctors/(:num)'] = "doctors/index/$1";
$route['services/(:num)'] = "services/index/$1";
$route['reviews/(:num)'] = "reviews";
$route['404_override'] = '';
$route['add_areas'] = 'areas/add';
$route['add_city'] = 'cities/add';
$route['pages'] = "pages/index";
$route['pages/save/(:num)'] = "pages/save";
$route['remove_page'] = "pages/remove_page";



/* End of file routes.php */
/* Location: ./application/config/routes.php */

/* start todary route */
$route['allCampanies'] = "companies/all_companies";
$route['profile/(:num)'] = "companies/profile/$1";
$route['activeCompany/(:num)'] = "companies/active_company/$1";
$route['remove/(:num)'] = "Companies/remove_company/$1";

/* end todary route  */