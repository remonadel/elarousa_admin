<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Categories extends MX_Controller {

	public function __construct()
    {
        parent::__construct();
		$this->deny->deny_if_logged_out();
		
		$this->load->model("categories_model");
    }
	
	
	public function index()
	{

		$data = array();
		$current_page = (int) $this->uri->segment(2);
		$per_page = 10;
		$questions_count = $this->categories_model->get_table_rows_count();
		$config["base_url"] = site_url() . "categories/";
		$config['uri_segment'] = 2;
		$config["total_rows"] = $questions_count;
		$config["per_page"] = $per_page;
		$this->pagination->initialize($config);
		$data["pagination"] = $this->pagination->create_links();
		$services = $this->categories_model->get_all($per_page, $current_page);
		foreach ($services as &$service)
        {
            $service['companies_count'] = $questions_count = $this->categories_model->get_category_companies_count($service['category_id']);
        }

		if ($services)
		{
			$data["doctors"] = $services;
		}

		$this->load->view("manage_categories_view", $data);

	}
	
	
	public function add()
	{
		$data = array();
		$data['categories'] = $this->common_model->get_all_from_table("categories");

		if (isset($_POST["submit"]))
		{
			$name = htmlspecialchars(trim($_POST["name"]));
			$image_name = @$_FILES["image"]["name"];

			if (empty($name) OR empty($image_name))
			{
				$data["status"] = "<p class='error-msg'>يجب إدخال جميع البيانات الإجبارية</p>";
			}
			elseif ($this->common_model->subject_exists("categories", "category_name", $name))
			{
				$data["status"] = "<p class='error-msg'>هذه الخدمة موجودة بالفعل</p>";
			}
			else
			{
				if (!empty($image_name))
				{
					$tmp_picture_name = $_FILES["image"]["tmp_name"];
					$allowed_exts = array("jpg", "png");
					$a_long = explode(".", $image_name);
					$file_ext = strtolower(end($a_long));
					unset($a_long);
					$path = CATEGORIES_PHOTOS_PATH;
					// Restricting file uploading to zip files only
					if (!in_array($file_ext, $allowed_exts))
					{
						$data["status"] = "<p class='error-msg'>لقد قمت برفع ملف غير مسموح به</p>";
					}
					else
					{
						// Success. Give the pictures a unique (same) name and upload them, then insert data and log action
						$cur_time = time();
						$picture_name = $cur_time . "." . $file_ext;
						move_uploaded_file($tmp_picture_name, $path . $picture_name);
						$insert_id = $this->categories_model->insert_category($name, $picture_name);
						$this->session->set_flashdata("status", "تمت العملية بنجاح");
						redirect(site_url() . "categories");
					}
				}

			}
		}

		$this->load->view("add_category_view", $data);

	}


	public function areas_of_city()
	{
		$city_id = $_POST['city'];
		$areas = $this->doctors_model->get_areas_of_city($city_id);
		echo json_encode($areas);
	}
	
	
	public function edit($id = "")
	{
		$service = $this->common_model->get_subject_with_token("categories", "category_id", $id);
		if (empty($id) OR ! $service) redirect(site_url() . "categories");

		$data = array();
		$data['doctor'] = $service;

		if (isset($_POST["submit"]))
		{
			$name = htmlspecialchars(trim($_POST["name"]));
			$image_name = @$_FILES["image"]["name"];

			if (empty($name))
			{
				$data["status"] = "<p class='error-msg'>يجب إدخال جميع البيانات الإجبارية</p>";
			}
			else
			{
				if (!empty($image_name))
				{
					$tmp_picture_name = $_FILES["image"]["tmp_name"];
                    $allowed_exts = array("jpg", "png");
					$a_long = explode(".", $image_name);
					$file_ext = strtolower(end($a_long));
					unset($a_long);
					$path = CATEGORIES_PHOTOS_PATH;
					// Restricting file uploading to zip files only
					if (!in_array($file_ext, $allowed_exts))
					{
						$data["status"] = "<p class='error-msg'>لقد قمت برفع ملف غير مسموح به</p>";
					}
					else
					{
						// Success. Give the pictures a unique (same) name and upload them, then insert data and log action
						$cur_time = time();
						$picture_name = $cur_time . "." . $file_ext;
						move_uploaded_file($tmp_picture_name, $path . $picture_name);
						$insert_id = $this->categories_model->edit_category($id, $name, $picture_name);
						$this->session->set_flashdata("status", "تمت العملية بنجاح");
						redirect(site_url() . "categories");
					}
				}
				else
				{
					// Success. Give the pictures a unique (same) name and upload them, then insert data and log action
					$cur_time = time();
					$insert_id = $this->categories_model->edit_category($id, $name, '');
					$this->session->set_flashdata("status", "تمت العملية بنجاح");
					redirect(site_url() . "categories");
				}

			}
		}

		$this->load->view("edit_category_view", $data);

	}

	
	
	public function delete($id = "")
    {	

		$service = $this->common_model->get_subject_with_token("categories", "category_id", $id);
		if (empty($id) OR ! $service) redirect(site_url() . "services");

		$this->common_model->delete_subject("categories", "category_id", $id);

		$this->session->set_flashdata("status", "تمت العملية بنجاح");
		redirect($_SERVER['HTTP_REFERER']);

	}
	


}


/* End of file sections.php */
/* Location: ./application/modules/sections/controllers/sections.php */