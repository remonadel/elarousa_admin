<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Categories_model extends CI_Model {
    
    public function __construct()
    {
        parent::__construct();
    }
    
	
	public function get_active_sections()
	{
		$sql = "SELECT * FROM `sections` WHERE `active` = 1 ORDER BY `arrange_flag` ASC";
		$query = $this->db->query($sql);
		
		if ($query->num_rows() >= 1)
		{			
			return $query->result_array();
		}
		else
		{
			return FALSE;
		}
	}

	public function get_areas_of_city($city_id)
	{
		$this->db->where('city_id', $city_id);
		$query = $this->db->get('areas');
		return $query->result_array();
	}


	public function get_all($limit, $offset)
	{
		$this->db->order_by("category_id", 'desc');
		$query = $this->db->get('categories', $limit, $offset);
		return $query->result_array();
	}

	public function get_table_rows_count()
	{
		/* Returns count of all rows in a table. Returns a number. */

		$sql = "SELECT COUNT(`category_id`) AS `count` FROM `categories`";
		$query = $this->db->query($sql);
		$count = $query->row_array();
		return $count["count"];
	}

    public function get_category_companies_count($category_id)
    {
        /* Returns count of all rows in a table. Returns a number. */

        $sql = "SELECT COUNT(`comp_id`) AS `count` FROM `company` WHERE category_id = $category_id";
        $query = $this->db->query($sql);
        $count = $query->row_array();
        return $count["count"];
    }

	public function insert_category($name, $picture_name)
    {
       $this->db->insert('categories', array('category_name' => $name, 'category_image' => $picture_name));

    }

	public function edit_category($id, $name, $picture_name)
	{
		$this->db->where("category_id", $id);
		if (!empty($picture_name))
		{
			$this->db->update('categories', array('category_name' => $name ,'category_image' => $picture_name));
		}
		else
		{
			$this->db->update('categories', array('category_name' => $name));
		}

	}

    

	

}


/* End of file sections_model.php */
/* Location: ./application/modules/sections/models/sections_model.php */