<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Areas_model extends CI_Model {
    
    public function __construct()
    {
        parent::__construct();
    }
    
	
	public function get_active_sections()
	{
		$sql = "SELECT * FROM `sections` WHERE `active` = 1 ORDER BY `arrange_flag` ASC";
		$query = $this->db->query($sql);
		
		if ($query->num_rows() >= 1)
		{			
			return $query->result_array();
		}
		else
		{
			return FALSE;
		}
	}


	public function get_all()
	{
		$query = $this->db->query('SELECT district.district_id, district.district_name, cities.city_name as city_name FROM district LEFT JOIN cities ON district.city_id=cities.city_id');
		return $query->result_array();
	}
	
    public function insert_area($name, $city)
    {
        $sql = "INSERT INTO `district` (`district_name`, `city_id`) VALUES (?, ?)";
        $query = $this->db->query($sql, array($name, $city));
        
        return $this->db->insert_id();
    }
    
    
    public function update_area($name, $city, $id)
    {
        $sql = "UPDATE `district` SET `district_name` = ?, `city_id` = ? WHERE `district_id` = ?";
        $query = $this->db->query($sql, array($name, $city, $id));
    }
    
    
    public function get_section_details($id)
	{
		$sql = "SELECT * FROM `sections` WHERE `id` = ?";
		$query = $this->db->query($sql, array($id));
		
		if ($query->num_rows() >= 1)
		{
			$row = $query->row_array();
            
			$subsections = $this->common_model->get_all_subjects_with_token("subsections", "section_id", $id);
            if ($subsections) $row["subsections"] = $subsections;
            
			return $row;
		}
	}
	

}


/* End of file sections_model.php */
/* Location: ./application/modules/sections/models/sections_model.php */