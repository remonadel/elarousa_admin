<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Areas extends MX_Controller {

	public function __construct()
    {
        parent::__construct();
		$this->deny->deny_if_logged_out();
		
		$this->load->model("areas_model");
    }
	
	
	public function index()
	{
        $data = array();

        $sections = $this->areas_model->get_all();
        if ($sections)
        {

            $data["sections"] = $sections;
        }

        $this->load->view("manage_areas_view", $data);

	}
	
	
	public function add()
	{
        $data = array();
        $data['cities'] = $this->common_model->get_all_from_table("cities");

        if (isset($_POST["submit"]))
        {
            $name = trim($_POST["name"]);
            $city = $_POST["city"];

            if (empty($name))
            {
                $data["status"] = "<p class='error-msg'>يجب إدخال إسم المنطقة </p>";
            }
            elseif ($this->common_model->subject_exists("district", "district_name", $name))
            {
                $data["status"] = "<p class='error-msg'>هذه المنطقة موجودة بالفعل</p>";
            }
            else
            {
                // Success. Insert info then log action
                $insert_id = $this->areas_model->insert_area($name, $city);

                $this->session->set_flashdata("status", "تمت العملية بنجاح");
                redirect(site_url() . "areas");
            }
        }

        $this->load->view("add_area_view", $data);

	}
	
	
	public function view($id = "")
	{	
		$authorized = $this->common_model->authorized_to_view_page("admin_permissions");
		if ($authorized)
		{
			$section = $this->common_model->get_subject_with_token("sections", "id", $id);
			if (empty($id) OR ! $section) show_404();
			
			$data["section"] = $section;
			
			echo $this->load->view("ajax_view_section_view", $data, TRUE);
		}
	}
	
	
	public function edit($id = "")
	{
        $data['cities'] = $this->common_model->get_all_from_table("cities");
        $area = $this->common_model->get_subject_with_token("district", "district_id", $id);
        if (empty($id) OR ! $area) show_404();

        $data["area"] = $area;

        if (isset($_POST["submit"]))
        {
            $name = trim($_POST["name"]);
            $city = $_POST["city"];

            if (empty($name))
            {
                $data["status"] = "<p class='error-msg'>يجب إدخال اسم المنطقة</p>";
            }
            else
            {
                $this->areas_model->update_area($name, $city, $id);

                $this->session->set_flashdata("status", "تمت العملية بنجاح");
                redirect(site_url() . "areas");
            }
        }

        $this->load->view("edit_area_view", $data);

	}
	
	
	public function delete($id = "")
    {
        $area = $this->common_model->get_subject_with_token("district", "district_id", $id);
        if (empty($id) OR ! $area) redirect(site_url() . "areas");

        $this->common_model->delete_subject("district", "district_id", $id);

        $this->session->set_flashdata("status", "تمت العملية بنجاح");
        redirect($_SERVER['HTTP_REFERER']);

	}
	


}


/* End of file sections.php */
/* Location: ./application/modules/sections/controllers/sections.php */