<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

class Company_model extends CI_Model
{


    protected $table_name = "company";
    protected $primary_key = "comp_id";
    protected $fields = "";
    protected $order_by = "";
    protected $primary_filter = "intval";
    protected $timestamps = false;


    public function __construct()
    {
        parent::__construct();
    }

    public function get($id = null, $single = FALSE){

        if($id != null)
        {
            $filter = $this->primary_filter;
            $id = $filter($id); // intval($id)
            $this->db->where($this->primary_key,$id);
            $method = 'row';
        }
        else if($single == TRUE)
        {
            $method = 'row';
        }

        else
        {
            $method = 'result';
        }


        return $this->db->get($this->table_name)->$method();
    }

    // frunction that select single row or all result from table name of specific condition
    public function get_by($where , $single = FALSE){

        $this->db->where($where);
        return $this->get(null , $single);
    }

    public function get_all_companies()
    {
        return $this->db->query("SELECT `comp_id`,`comp_name`,`comp_active` FROM `$this->table_name` ")->result();
    }


    public function update($id,$data)
    {
        $this->db->where($this->primary_key, $id);
        return $this->db->update($this->table_name, $data);
    }
    public function delete($id,$data){
        $this->db->delete($this->table_name, array('comp_id' => $id));
        $this->db->delete('company_services', array('company_id' => $id));
        $this->db->delete('company_portofolio', array('comp_id' => $id));
    }



    public function get_company_by_id($company_id)
    {
        $this->db->join('district', 'district.district_id = company.comp_district_id', 'left');
        $this->db->join('cities', 'cities.city_id = company.comp_city_id', 'left');
        $this->db->join('categories', 'categories.category_id = company.category_id', 'left');
        $this->db->where('comp_id', $company_id);
        $query = $this->db->get($this->table_name);
        $users = $query->row();

        if (count($users) && !empty($users) && isset($users)) {
            return $users;//if user exist
        } else {

            return false;//if user not exist
        }
    }



}


/* End of file sections_model.php */
/* Location: ./application/modules/sections/models/sections_model.php */