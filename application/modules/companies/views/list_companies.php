<?php $this->load->view("header"); ?>
    <input type="hidden" class="url_class" value="<?= base_url() ?>">

    <div style="padding-top: 100px;">
        <div class="container">
            <div class="row centered">
                <div class="col-md-12">


                    <div class="panel-body">
                        <table width="100%" class="table table-striped table-bordered table-hover" id="dataTables-example">
                            <thead>
                            <tr>
                                <th> اسم الشركه </th>
                                <th> شاهد الشركه  </th>
                                <th> اكتف الشركه  </th>
                                <th> مسح الشركه  </th>
                            </tr>
                            </thead>
                            <tbody>
                            <?php foreach($comp_data as $key => $value): ?>

                                <tr class="odd gradeX">
                                    <td class="center"><?=$value->comp_name?></td>
                                    <td class="center"><a class="btn btn-success" target="_blank" href="<?= ROOT2.'profile/'.$value->comp_id?>?admin=1">شاهد</a></td>
                                    <td class="center">
                                        <a class="btn <?=($value->comp_active)?'btn-success':'btn-danger'?>" href="<?= site_url().'activeCompany/'.$value->comp_id?>?active=<?=$value->comp_active?>">الحاله</a></td>
                                    </td>
                                    <td class="center">
                                        <a class="btn btn-danger" href="<?= site_url().'remove/'.$value->comp_id?>">مسح</a></td>
                                    </td>
                                </tr>
                            <?php endforeach;?>
                            </tbody>
                        </table>
                        <!-- /.table-responsive -->


                </div>
            </div>
        </div>
    </div>

    <script src="<?= ASSETS; ?>js/jquery.dataTables.min.js"></script>
    <script src="<?= ASSETS; ?>js/dataTables.bootstrap.min.js"></script>
    <script>
        $(document).ready(function() {
            $('#dataTables-example').DataTable({
                responsive: true
            });
        });
    </script>
<?php $this->load->view("footer"); ?>