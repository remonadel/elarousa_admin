<?php

defined('BASEPATH') OR exit('No direct script access allowed');


class Companies extends CI_Controller
{
    public $data = array();

    public function __construct()
    {
        parent::__construct();
        $this->load->model("Company_model");

    }



    public function check_post($arr_keys, $method = "post")
    {
        $data = [];
        foreach ($arr_keys as $item) {
            $data[$item] = "";
            if ($this->input->$method($item)) {
                $data[$item] = $this->input->$method($item);
            }
        }

        return $data;
    }


    public function all_companies(){

        $this->data['comp_data'] = $this->Company_model->get_all_companies();
        $this->load->view('list_companies', $this->data);
    }


    public function remove_company($id){
        $this->Company_model->delete($id);
        redirect('allCampanies');
    }



    public function active_company($id){

        $company=$this->Company_model->get($id);
        if ($_GET['active'])
            $active=0;
        else {
            $active=1;
            // send email
            $email_subject = "تم تفعيل حسابك ";
            $email_body = " <h1 style='text-align: center'>تم تفعيل حسابك بنجاح يمكن الدخول الان </h1> ";
            $verification_url = ROOT2 ;
            $email_body .= " <a style='font-size:21px;background-color:green;color:#fff;padding:5px 5px;text-decoration: none;' href='".$verification_url."'> تم تفعيل </a> ";

            $this->_send_email_to_user($email_subject,$email_body,$company->comp_email);

        }

        $result=$this->Company_model->update($id,[
            "comp_active" => $active
        ]);
        if ($result)
            redirect('allCampanies');

    }


    private function _send_email_to_user($subject,$body,$email) {
        $to      = $email;
        $message = $body;
        $headers = 'From: noreply@el3arousa.com' . "\r\n" .
            'Reply-To: noreply@el3arousa.com' . "\r\n" .
            'X-Mailer: PHP/' . phpversion();
        $headers .= "MIME-Version: 1.0\r\n";
        $headers .= "Content-Type: text/html; charset=ISO-8859-1\r\n";

        mail($to, $subject, $message, $headers);
    }


}