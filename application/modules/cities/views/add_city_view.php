<?php $this->load->view("header"); ?> 

<div class="container">
	<div class="col-md-12">
		<div class="main-title">
			<h1> إضافة مدينة</h1>
		</div>
	</div>
	<div class="row">
		<div class="masonary-grids">
			<div class="col-md-12">
				<div class="widget-area">
					<div class="wizard-form-h">
						<form action="add_city" method="post">
							<div class="col-md-122">
								<div class="inline-form">
									<label class="c-label"> اسم المدينة</label>
									<input class="input-style" type="text" name="name" required autofocus />
								</div>
							</div>
                            <div class="col-md-122">
                                <div class="inline-form">
                                    <label class="c-label">البلد</label>
                                    <select name="country" required>
                                        <option></option>
                                        <?php foreach($countries as $country): ?>

                                            <option value="<?= $country['country_id'] ?>"> <?= $country['country_name'] ?></option>
                                        <?php endforeach; ?>
                                    </select>

                                </div>
                            </div>


							<div class="col-md-62" style="margin-top: 12px;">
								<input type="submit" name="submit" value="حفظ" class="btn btn-success btn-font" />
							</div>
							<div class="col-md-122" style="margin-top: 12px;">
								<?php if (isset($status)) echo $status; ?> 
							</div>
						</form>
					</div>
				</div>
			</div>
		</div>
	</div>
	<?php //$this->load->view("slide_panel"); ?> 
</div><!-- Page Container -->
<?php $this->load->view("footer"); ?> 
</body>
</html>