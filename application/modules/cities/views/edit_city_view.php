<?php $this->load->view("header"); ?> 

<div class="container">
	<div class="title-date-range">
		<div class="row">
			<div class="col-md-6">
			</div>
		</div>
	</div><!-- title Date Range -->
	<div class="col-md-12">
		<div class="main-title">
			<h1>تعديل مدينة </h1>
		</div>
	</div>
	<div class="row">
		<div class="masonary-grids">
			<div class="col-md-12">
				<div class="widget-area">
					<div class="wizard-form-h">
						<form action="" method="post">
							<div class="col-md-122">
								<div class="inline-form">
									<label class="c-label">إسم المدينة</label>
									<input class="input-style" type="text" name="name"
										   value="<?= $section["city_name"]; ?>" required autofocus />
								</div>
							</div>


							<div class="col-md-62" style="margin-top: 12px;">
								<input type="submit" name="submit" value="Save" class="btn btn-success large" />
							</div>
							<div class="col-md-122" style="margin-top: 12px;">
								<?php if (isset($status)) echo $status; ?> 
							</div>
						</form>
					</div>
				</div>
			</div>
		</div>
	</div>
	<?php //$this->load->view("slide_panel"); ?> 
</div><!-- Page Container -->
<?php $this->load->view("footer"); ?> 
</body>
</html>