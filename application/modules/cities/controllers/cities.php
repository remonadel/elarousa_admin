<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Cities extends MX_Controller {

	public function __construct()
    {
        parent::__construct();
		$this->deny->deny_if_logged_out();
		
		$this->load->model("cities_model");
    }
	
	
	public function index()
	{

		$data = array();

		$cities = $this->cities_model->get_all();

		if ($cities)
		{

			$data["sections"] = $cities;
		}

		$this->load->view("manage_cities_view", $data);

	}
	
	
	public function add()
	{	

		$data = array();
        $data['countries'] = $this->common_model->get_all_from_table("country");

		if (isset($_POST["submit"]))
		{
			$name = trim($_POST["name"]);
            $country = $_POST["country"];

			if (empty($name))
			{
				$data["status"] = "<p class='error-msg'> يرجي إدخال اسم المحافظة</p>";
			}
			elseif ($this->common_model->subject_exists("cities", "city_name", $name))
			{
				$data["status"] = "<p class='error-msg'>هذا الاسم موجود بالفعل</p>";
			}
			else
			{
				// Success. Insert info then log action
				$insert_id = $this->cities_model->insert_city($name,$country);


				$this->session->set_flashdata("status", "تمت العملية بنجاح");
				redirect(site_url() . "cities");
			}
		}

		$this->load->view("add_city_view", $data);

	}
	
	
	public function view($id = "")
	{	
		$authorized = $this->common_model->authorized_to_view_page("admin_permissions");
		if ($authorized)
		{
			$section = $this->common_model->get_subject_with_token("sections", "id", $id);
			if (empty($id) OR ! $section) show_404();
			
			$data["section"] = $section;
			
			echo $this->load->view("ajax_view_section_view", $data, TRUE);
		}
	}
	
	
	public function edit($id = "")
	{	

		$city = $this->common_model->get_subject_with_token("cities", "city_id", $id);
		if (empty($id) OR ! $city) show_404();

		$data["section"] = $city;

		if (isset($_POST["submit"]))
		{
			$name = trim($_POST["name"]);

			if (empty($name))
			{
				$data["status"] = "<p class='error-msg'>يجب إدخال إسم المحافظة</p>";
			}
			else
			{
				// Success.

				$this->cities_model->update_city($name, $id);

				$this->session->set_flashdata("status", "تمت العملية بنجاح");
				redirect(site_url() . "cities");
			}
		}

		$this->load->view("edit_city_view", $data);

	}
	
	
	public function delete($id = "")
    {	

		$city = $this->common_model->get_subject_with_token("cities", "city_id", $id);

		if (empty($id) OR ! $city) redirect(site_url() . "cities");

		$this->common_model->delete_subject("cities", "city_id", $id);

		$this->session->set_flashdata("status", "تمت العملية بنجاح");
		redirect($_SERVER['HTTP_REFERER']);

	}
	


}


/* End of file sections.php */
/* Location: ./application/modules/sections/controllers/sections.php */