<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Services extends MX_Controller {

	public function __construct()
    {
        parent::__construct();
		$this->deny->deny_if_logged_out();
		
		$this->load->model("services_model");
    }
	
	
	public function index()
	{

		$data = array();
		$current_page = (int) $this->uri->segment(2);
		$per_page = 10;
		$questions_count = $this->services_model->get_table_rows_count();
		$config["base_url"] = site_url() . "services/";
		$config['uri_segment'] = 2;
		$config["total_rows"] = $questions_count;
		$config["per_page"] = $per_page;
		$this->pagination->initialize($config);
		$data["pagination"] = $this->pagination->create_links();
		$services = $this->services_model->get_all($per_page, $current_page);
		if ($services)
		{
			$data["doctors"] = $services;
		}

		$this->load->view("manage_doctors_view", $data);

	}
	
	
	public function add()
	{
		$data = array();
		$data['categories'] = $this->common_model->get_all_from_table("categories");

		if (isset($_POST["submit"]))
		{
			$name = htmlspecialchars(trim($_POST["name"]));
			$category = $_POST['category'];
			$description = @htmlspecialchars(trim($_POST['description']));
			$image_name = @$_FILES["image"]["name"];

			if (empty($name) OR empty($category))
			{
				$data["status"] = "<p class='error-msg'>يجب إدخال جميع البيانات الإجبارية</p>";
			}
			elseif ($this->common_model->subject_exists("services", "service_name", $name))
			{
				$data["status"] = "<p class='error-msg'>هذه الخدمة موجودة بالفعل</p>";
			}
			else
			{
				if (!empty($image_name))
				{
					$tmp_picture_name = $_FILES["image"]["tmp_name"];
					$allowed_exts = array("jpg");
					$a_long = explode(".", $image_name);
					$file_ext = strtolower(end($a_long));
					unset($a_long);
					$path = SERVICES_PHOTOS_PATH;
					// Restricting file uploading to zip files only
					if (!in_array($file_ext, $allowed_exts))
					{
						$data["status"] = "<p class='error-msg'>لقد قمت برفع ملف غير مسموح به</p>";
					}
					else
					{
						// Success. Give the pictures a unique (same) name and upload them, then insert data and log action
						$cur_time = time();
						$picture_name = $cur_time . "." . $file_ext;
						move_uploaded_file($tmp_picture_name, $path . $picture_name);
						$insert_id = $this->services_model->insert_service($name, $category, $description, $picture_name);
						$this->session->set_flashdata("status", "تمت العملية بنجاح");
						redirect(site_url() . "services");
					}
				}
				else
				{
					// Success. Give the pictures a unique (same) name and upload them, then insert data and log action
					$cur_time = time();
					$insert_id = $this->services_model->insert_service($name, $category, $category, $description, '');
					$this->session->set_flashdata("status", "تمت العملية بنجاح");
					redirect(site_url() . "services");
				}

			}
		}

		$this->load->view("add_doctor_view", $data);

	}


	public function areas_of_city()
	{
		$city_id = $_POST['city'];
		$areas = $this->doctors_model->get_areas_of_city($city_id);
		echo json_encode($areas);
	}
	
	
	public function edit($id = "")
	{
		$service = $this->common_model->get_subject_with_token("services", "service_id", $id);
		if (empty($id) OR ! $service) redirect(site_url() . "services");

		$data = array();
		$data['doctor'] = $service;
		$data['categories'] = $this->common_model->get_all_from_table("categories");

		if (isset($_POST["submit"]))
		{
			$name = htmlspecialchars(trim($_POST["name"]));
			$category = $_POST['category'];
			$description = @htmlspecialchars(trim($_POST['description']));
			$image_name = @$_FILES["image"]["name"];

			if (empty($name) OR empty($category))
			{
				$data["status"] = "<p class='error-msg'>يجب إدخال جميع البيانات الإجبارية</p>";
			}
			else
			{
				if (!empty($image_name))
				{
					$tmp_picture_name = $_FILES["image"]["tmp_name"];
					$allowed_exts = array("jpg");
					$a_long = explode(".", $image_name);
					$file_ext = strtolower(end($a_long));
					unset($a_long);
					$path = SERVICES_PHOTOS_PATH;
					// Restricting file uploading to zip files only
					if (!in_array($file_ext, $allowed_exts))
					{
						$data["status"] = "<p class='error-msg'>لقد قمت برفع ملف غير مسموح به</p>";
					}
					else
					{
						// Success. Give the pictures a unique (same) name and upload them, then insert data and log action
						$cur_time = time();
						$picture_name = $cur_time . "." . $file_ext;
						move_uploaded_file($tmp_picture_name, $path . $picture_name);
						$insert_id = $this->services_model->edit_service($id, $name, $category, $description, $picture_name);
						$this->session->set_flashdata("status", "تمت العملية بنجاح");
						redirect(site_url() . "services");
					}
				}
				else
				{
					// Success. Give the pictures a unique (same) name and upload them, then insert data and log action
					$cur_time = time();
					$insert_id = $this->services_model->edit_service($id, $name, $category, $description, '');
					$this->session->set_flashdata("status", "تمت العملية بنجاح");
					redirect(site_url() . "services");
				}

			}
		}

		$this->load->view("edit_doctor_view", $data);

	}

	
	
	public function delete($id = "")
    {	

		$service = $this->common_model->get_subject_with_token("services", "service_id", $id);
		if (empty($id) OR ! $service) redirect(site_url() . "services");

		$this->common_model->delete_subject("services", "service_id", $id);

		$this->session->set_flashdata("status", "تمت العملية بنجاح");
		redirect($_SERVER['HTTP_REFERER']);

	}
	


}


/* End of file sections.php */
/* Location: ./application/modules/sections/controllers/sections.php */