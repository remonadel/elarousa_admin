<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Services_model extends CI_Model {
    
    public function __construct()
    {
        parent::__construct();
    }
    
	
	public function get_active_sections()
	{
		$sql = "SELECT * FROM `sections` WHERE `active` = 1 ORDER BY `arrange_flag` ASC";
		$query = $this->db->query($sql);
		
		if ($query->num_rows() >= 1)
		{			
			return $query->result_array();
		}
		else
		{
			return FALSE;
		}
	}

	public function get_areas_of_city($city_id)
	{
		$this->db->where('city_id', $city_id);
		$query = $this->db->get('areas');
		return $query->result_array();
	}


	public function get_all($limit, $offset)
	{
		$this->db->order_by("service_id", 'desc');
		$this->db->join('categories', 'categories.category_id = services.category_id');
		$query = $this->db->get('services', $limit, $offset);
		return $query->result_array();
	}

	public function get_table_rows_count()
	{
		/* Returns count of all rows in a table. Returns a number. */

		$sql = "SELECT COUNT(`service_id`) AS `count` FROM `services`";
		$query = $this->db->query($sql);
		$count = $query->row_array();
		return $count["count"];
	}


	public function insert_service($name, $category, $description, $picture_name)
    {
       $this->db->insert('services', array('service_name' => $name, 'category_id' => $category ,'service_image' => $picture_name, 'service_description' => $description));

    }

	public function edit_service($id, $name, $category, $description, $picture_name)
	{
		$this->db->where("service_id", $id);
		if (!empty($picture_name))
		{
			$this->db->update('services', array('service_name' => $name, 'category_id' => $category ,'service_image' => $picture_name, 'service_description' => $description));
		}
		else
		{
			$this->db->update('services', array('service_name' => $name, 'category_id' => $category , 'service_description' => $description));
		}

	}

    

	

}


/* End of file sections_model.php */
/* Location: ./application/modules/sections/models/sections_model.php */