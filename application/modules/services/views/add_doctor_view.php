<?php $this->load->view("header"); ?> 

<div class="container">
	<div class="col-md-12">
		<div class="main-title">
			<h1>إضافة خدمة </h1>
		</div>
	</div>
	<div class="row">
		<div class="masonary-grids">
			<div class="col-md-12">
				<div class="widget-area">
					<div class="wizard-form-h">
						<form action="" method="post" enctype="multipart/form-data">
							<div class="col-md-122">
								<div class="inline-form">
									<label class="c-label">إسم الخدمة</label>
									<input class="input-style" type="text" name="name" required autofocus />
								</div>
							</div>
							<div class="col-md-122">
								<div class="inline-form">
									<label class="c-label">إلصورة</label>
									<input class="input-style" type="file" name="image" required  autofocus />
								</div>
							</div>
							<div class="col-md-122">
								<div class="inline-form">
									<label class="c-label">التصنيف</label>
									<select name="category"  required>
										<option></option>
										<?php foreach($categories as $category): ?>
											<option value="<?= $category['category_id'] ?>"> <?= $category['category_name'] ?></option>
										<?php endforeach; ?>
									</select>

								</div>
							</div>


							<div class="col-md-122">
								<div class="inline-form">
									<label class="c-label">التفاصيل</label>
									<textarea name="description"  style="height: 80px;"></textarea>
								</div>
							</div>

							<div class="col-md-62" style="margin-top: 12px;">
								<input type="submit" name="submit" value="حفظ" class="btn btn-success btn-font" />
							</div>
							<div class="col-md-122" style="margin-top: 12px;">
								<?php if (isset($status)) echo $status; ?> 
							</div>
						</form>
					</div>
				</div>
			</div>
		</div>
	</div>
	<?php //$this->load->view("slide_panel"); ?> 
</div><!-- Page Container -->
<?php $this->load->view("footer"); ?> 
</body>
</html>
<script>
	$( "#city_select" ).change(function() {
		$.post( "<?= site_url()?>doctors/areas_of_city", {city: $( "#city_select" ).val()},function( data ) {
			var obj = jQuery.parseJSON(data);
			var html = "<option value=''>  اختر المنطقة</option>";
			$.each(obj, function( index, value ) {
				html += "<option value='"+value['id']+"'>"+value['name']+"</option>";
			});
			$('#area_select_div').html(html);
		});
	});
</script>