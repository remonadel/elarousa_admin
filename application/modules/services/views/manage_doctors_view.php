<?php $this->load->view("header"); ?> 

<div class="container">
	<div class="col-md-12">
		<div class="main-title">
			<h1>إدارة الخدمات</h1>
		</div>
	</div>
	<div class="row">
		<div class="masonary-grids">
			<div class="col-md-12">
				<div class="widget-area">
					<?php if ($this->session->flashdata("status")): ?> 
						<div class="col-md-122" id="status" style="background-color: #EEE; padding: 10px;"><p class="success-msg"><?= $this->session->flashdata("status"); ?></p></div>
					<?php endif; ?> 
					<div class="streaming-table">
						<span id="found" class="label label-info"></span>
						<table id="stream_table" class='table table-striped table-bordered'>
							<thead>
								<tr>
									<th>ID</th>
									<th>الاسم</th>
									<th>الصورة</th>
									<th>التصنيف</th>
									<th class="tables-15-width-th">تعديل</th>
									<th class="tables-15-width-th">حذف</th>
								</tr>
							</thead>
							<tbody class="tbody_admin">
								<?php if (isset($doctors)): ?>
								<?php foreach ($doctors as $doctor): ?>
								<tr>
									<td><?= $doctor["service_id"]; ?></td>
									<td class="tables-centered-both-td"><?= $doctor["service_name"]; ?></td>
									<td class="tables-centered-both-td">
										<?php if(!empty($doctor["service_image"])): ?>
										<img src="<?= SERVICES_PHOTOS.$doctor["service_image"]; ?>" width="80" hight="120">
										<?php endif; ?>
									</td>
									<td class="tables-centered-both-td"><?= $doctor["category_name"]; ?></td>

									<td>
										<?php //if ($this->session->userdata("doctor_permissions") == 1): ?>
											<a href="<?= site_url(); ?>services/edit/<?= $doctor['service_id']; ?>">
												<button class="btn btn-warning btn-font tables-full-width-btn" type="button">تعديل</button>
											</a>
<!--										--><?php //else: ?>
<!--											<a>-->
<!--												<button class="btn btn-warning btn-font tables-full-width-btn no-permission-btn" type="button" title="لا تملك صلاحيات كافية">تعديل</button>-->
<!--											</a>-->
<!--										--><?php //endif; ?><!-- -->
									</td>
									<td>
										<?php //if ($this->session->userdata("doctor_permissions") == 1): ?>

												<a onclick="alertDelete('services/delete/<?= $doctor['service_id']; ?>', 'هل أنت متأكد من حذف هذه الخدمة؟');" href="javascript:void(null);">
													<button class="btn btn-danger btn-font tables-full-width-btn" type="button" >حذف</button>
												</a>
<!---->
<!--										--><?php //else: ?>
<!--											<a>-->
<!--												<button class="btn btn-danger btn-font tables-full-width-btn no-permission-btn" type="button" title="لا تملك صلاحيات كافية">حذف</button>-->
<!--											</a>-->
<!--										--><?php //endif; ?><!-- -->
									</td>
								</tr>
								<?php endforeach; ?>
								<?php endif; ?> 
							</tbody>
						</table>
					</div>
					<?php if (isset($pagination)): ?>
						<div class="pagination-news">
							<?= $pagination; ?>
						</div>
					<?php endif; ?>
				</div>
			</div>
		</div>
	</div>
	<?php //$this->load->view("slide_panel"); ?> 
</div><!-- Page Container -->
<?php $this->load->view("footer"); ?> 
</body>
</html>