<?php require_once(UPLOADS_PATH.'../application/libraries/recaptchalib.php') ?>
<!DOCTYPE html>
<html>
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Login</title>
    <!-- Styles -->
    <link rel="stylesheet" href="<?= ASSETS; ?>css/font-awesome.css" type="text/css" /><!-- Font Awesome -->
    <link rel="stylesheet" href="<?= ASSETS; ?>css/bootstrap.css" type="text/css" /><!-- Bootstrap -->
    <link rel="stylesheet" href="<?= ASSETS; ?>css/style.css" type="text/css" /><!-- Style -->
    <link rel="stylesheet" href="<?= ASSETS; ?>css/responsive.css" type="text/css" /><!-- Responsive -->
</head>
<body style="background-image: url('<?= ASSETS; ?>header_images/<?=rand(1, 5)?>.jpg');  background-size: 100%;">
    <div class="login-sec">
    	<div class="login">
    		<div class="login-form">
                <span><img src="<?= ASSETS; ?>images/logo.png" alt="Elarousa logo" style="margin-bottom: 24px; border-radius: 10px;" /></span>
    			<form action="" method="post">
                    <fieldset>
                        <input type="text" name="username" placeholder="Username" style="text-align: left;"
							   value="<?php if (isset($_POST["username"])) echo htmlspecialchars(trim($_POST["username"])); ?>" required autofocus />
                        <i class="fa fa-user"></i>
                    </fieldset>
    				<fieldset>
                        <input type="password" name="password" placeholder="Password" style="text-align: left;" required />
                        <i class="fa fa-unlock-alt"></i>
                    </fieldset>
					<?php if (isset($_COOKIE["invalid_login_attempts"]) && $_COOKIE["invalid_login_attempts"] > 5): ?> 
						<div style="margin-bottom: 15px; margin-top: 130px; margin-right: 23px;">
							<?php $publickey = "6LcDaQsTAAAAAHWL57zZExDmyJpQwuIvBLMciDYU";
								echo recaptcha_get_html($publickey);
							?>
						</div>
					<?php endif; ?> 
                    <button type="submit" name="submit" class="blue">دخول</button>
					<?php if (isset($error)): ?>
						<p class="error-msg" style="margin-top: 10px;"><?= $error; ?></p>
					<?php endif; ?>
    			</form>
    		</div>

    	</div>
    </div>
</body>
</html>