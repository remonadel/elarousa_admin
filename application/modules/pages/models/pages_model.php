<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Pages_model extends CI_Model {

    protected $table_name = "pages";
    protected $primary_key = "page_id";
    protected $fields = "";
    protected $order_by = "page_id";

	public function __construct()
    {
        parent::__construct();
    }


    public function getPages($cond = "")
    {

        return $this->db->query(" 
          select *
          
          from pages
          $cond
          ")->result();

    }

    public function save($data)
    {
        $this->db->insert($this->table_name,$data);
        return $this->db->insert_id();
    }

    public function update($id,$data)
    {
        $this->db->where($this->primary_key, $id);
        $this->db->update($this->table_name, $data);
    }

    public function delete($page_id)
    {
        $this->db->query("delete from $this->table_name where  `page_id` = $page_id ");
    }

}

