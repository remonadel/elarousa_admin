<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Pages extends CI_Controller {

    public function __construct()
    {
        parent::__construct();

        $this->load->model("pages_model");
        $this->load->helper("img_processing");
        session_start();

        //print_r($this->session->all_userdata()); exit;
        //$this->session->sess_destroy();
    }


    public function index()
    {
        $this->deny->deny_if_logged_out();
        $data = array();

        $data["pages"] = $this->pages_model->getPages();

        $this->load->view("show_pages",$data);
    }


    public function save($page_id = null)
    {
        $this->deny->deny_if_logged_out();
        $data = array();

        $data["page_data"] = "";
        $page_id = $this->uri->segment(3);

        if ($page_id != null)
        {
            $data["page_data"] = $this->pages_model->getPages(" where page_id = $page_id ");
            if (!count($data["page_data"]) || !isset($data["page_data"][0]))
            {
                redirect(site_url("pages"));
            }
            $data["page_data"] = $data["page_data"][0];
        }

        $validation_rules = (array(
            "page_title" => array(
                "field" => "page_title",
                "rules" => "xss_clean|trim|required"
            ),
            "page_slug" => array(
                "field" => "page_slug",
                "rules" => "xss_clean|trim|required|callback__check_unique_slug[$page_id]"
            )
        ));

        $this->load->library("form_validation");
        $this->form_validation->set_rules($validation_rules);

        if ($this->form_validation->run())
        {

            $save_data = $this->check_post([
                "page_title", "page_slug", "page_body", "page_meta_title", "page_meta_desc",
                "page_meta_keywords"
            ]);

            $save_data['page_slug'] = $this->string_safe($save_data['page_slug']);

            if ($page_id != null)
            {
                // update
                $this->pages_model->update($page_id,$save_data);
            }
            else{
                // insert
                $page_id = $this->pages_model->save($save_data);
            }
            $data["success_msg"] = "لقد تم الحفظ بنجاح";
            $data["page_data"] = $this->pages_model->getPages(" where page_id = $page_id ");
            $data["page_data"] =  $data["page_data"][0];
        }


        $this->load->view("save_page",$data);
    }

    public function _check_unique_slug($str,$page_id)
    {
        $field_value = $str;
        $field_value = $this->string_safe($field_value);


        if (!empty($page_id))
        {
            $page_id = intval($page_id);
            $page = $this->pages_model->getPages(" where page_slug = '$field_value' and page_id <> $page_id ");
        }
        else{
            $page = $this->pages_model->getPages(" where page_slug = '$field_value' ");
        }

        if(!count($page))
        {
            return TRUE;
        }
        else
        {
            $this->form_validation->set_message('_check_unique_slug','عنوان الصفحه موجود مسبقا برجاء تغييرة');
            return FALSE;
        }
    }

    function string_safe($string) {
        $except = array('\\', '/', ':', '*', '?', '"', '<', '>', '|',' ','+','&');
        return str_replace($except, '', $string);
    }

    public function check_post($arr_keys, $method = "post")
    {
        $data = [];
        foreach ($arr_keys as $item) {
            $data[$item] = "";
            if ($this->input->$method($item)) {
                $data[$item] = $this->input->$method($item);
            }
        }

        return $data;
    }

    public function remove_page()
    {

        if ($this->input->server('REQUEST_METHOD') == 'POST')
        {

            $output["success"] = "";

            $page_id = $_POST['id'];
            $check = $this->pages_model->getPages(" 
                where page_id = $page_id
             ");
            if (!count($check))
            {
                $output["success"] = "هذا الحجز غير موجود !!";
                echo json_encode($output);
                return;
            }

            $this->pages_model->delete($page_id);
            $output["success"] = "success";
            echo json_encode($output);
            return;
        }

    }



    public function logout()
    {
        $this->deny->deny_if_logged_out();

        $this->session->sess_destroy();
        session_destroy();

        redirect(site_url() . "login");
    }


    public function change_password()
    {
        $this->deny->deny_if_logged_out();
        $data = array();

        if (isset($_POST["submit"]))
        {
            $password = htmlspecialchars(trim($_POST["password"]));
            $confirm_password = htmlspecialchars(trim($_POST["confirm_password"]));

            if (empty($password) OR empty($confirm_password))
            {
                $data["status"] = "<p class='error-msg'>يجب إدخال جميع البيانات الإجبارية (*)</p>";
            }
            elseif ($password !== $confirm_password)
            {
                $data["status"] = "<p class='error-msg'>تأكيد كلمة السر لم يطابق كلمة السر</p>";
            }
            elseif (strlen($password) < 6)
            {
                $data["status"] = "<p class='error-msg'>يجب ألا تقل كلمة السر عن 6 حروف</p>";
            }
            else
            {

                $this->home_model->update_user_password($password, $this->session->userdata("id"));

                $data["status"] = "<p class='success-msg'>تم تغيير كلمة السر بنجاح</p>";
            }
        }

        $this->load->view("change_password_view", $data);
    }


    public function change_picture()
    {
        $this->deny->deny_if_logged_out();
        $data = array();

        if (isset($_POST["submit"]))
        {
            $picture_name = basename($_FILES["picture"]["name"]);
            $w = $_POST["w"];
            $h = $_POST["h"];
            $x1 = $_POST["x1"];
            $y1 = $_POST["y1"];
            $tmp_name = $_FILES["picture"]["tmp_name"]; // getting the temporary file name
            $allowed_exts = array("jpg", "JPG", "jpeg", "JPEG", "png", "PNG", "gif", "GIF"); // specifying the allowed extensions
            $a = explode(".", $picture_name);
            $file_ext = strtolower(end($a)); unset($a); // getting the allowed extensions
            $path = USER_PHOTOS_PATH; // folder we store the employees photos in

            if (empty($picture_name))
            {
                $data["status"] = "<p class='error-msg'>لم تقوم بإختيار صورة</p>";
            }
            else
            {
                // Restricting file uploading to image files only
                if ( ! in_array($file_ext, $allowed_exts))
                {
                    $data["status"] = "<p class='error-msg'>يجب ان تكون الصورة من انواع ملفات الصور</p>";
                }
                else
                {
                    // Success. First delete the old picture then make new one
                    @unlink($path . $this->session->userdata("picture"));
                    $picture_name = $this->session->userdata("id") . "_" . time() . "." . $file_ext;
                    move_uploaded_file($tmp_name, $path . $picture_name);

                    // Now crop then resize the picture
                    custom_image_crop($path . $picture_name, $path . $picture_name, $x1, $y1, $w, $h);
                    custom_image_resize($path . $picture_name, $path . $picture_name, 250, 250);


                    $this->home_model->update_user_picture($picture_name, $this->session->userdata("id"));
                    $this->session->set_userdata("picture", $picture_name);

                    $this->session->set_flashdata("status", "<p class='success-msg'>تم تغيير الصورة بنجاح</p>");
                    redirect(site_url() . "change_picture");
                }
            }
        }

        $this->load->view("change_picture_view", $data);
    }

}


/* End of file home.php */
/* Location: ./application/modules/home/controllers/home.php */