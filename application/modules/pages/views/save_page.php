<?php $this->load->view("header"); ?>


<script src="https://cdn.ckeditor.com/4.7.0/standard/ckeditor.js"></script>

<div class="container">
    <div class="col-md-12">
        <div class="main-title">
            <h1>الرئيسية</h1>
        </div>
    </div>

    <div class="col-md-12">

        <?php

            $page_id = "";
            $page_title = "";
            $page_slug = "";
            $page_body = "";
            $page_meta_title = "";
            $page_meta_desc = "";
            $page_meta_keywords = "";
            if (is_object($page_data))
            {
                $page_id = $page_data->page_id;
                $page_title = $page_data->page_title;
                $page_slug = $page_data->page_slug;
                $page_body = $page_data->page_body;
                $page_meta_title = $page_data->page_meta_title;
                $page_meta_desc = $page_data->page_meta_desc;
                $page_meta_keywords = $page_data->page_meta_keywords;
            }

        ?>

        <form class="" action="<?= site_url("pages/save/$page_id") ?>" method="post" enctype="multipart/form-data">

            <div class="col-md-12">
                <?php if(!empty(validation_errors())) : ?>
                <div class="alert alert-danger">
                    <?php echo validation_errors(); ?>
                </div>
                <?php endif; ?>
            </div>

            <div class="col-md-12">
                <?php if(isset($success_msg) && !empty($success_msg)) : ?>
                <div class="alert alert-success">
                    <?php echo $success_msg; ?>
                </div>
                <?php endif; ?>
            </div>

            <div class="col-md-12 profile_fields">
                <p class="p_title">العنوان</p>
                <input type="text" name="page_title" class="form-control"
                       value="<?= $page_title ?>" required />
            </div>
            <div class="col-md-12 profile_fields">
                <p class="p_title">اللينك (يجب ان لا يتكرر ولا يحتوي علي رموز او مسافات)</p>
                <input type="text" name="page_slug" class="form-control"
                       value="<?= $page_slug ?>" required />
            </div>
            <div class="col-md-12 profile_fields">
                <p class="p_title">محتوي الصفحه</p>
                <textarea name="page_body" id="page_body" required class="form-control" cols="30" rows="10"><?=$page_body?></textarea>
            </div>
            <div class="col-md-12 profile_fields">
                <p class="p_title">Meta Title</p>
                <textarea name="page_meta_title" id="" class="form-control" cols="30" rows="10"><?=$page_meta_title?></textarea>
            </div>
            <div class="col-md-12 profile_fields">
                <p class="p_title">Meta Description</p>
                <textarea name="page_meta_desc" id="" class="form-control" cols="30" rows="10"><?=$page_meta_desc?></textarea>
            </div>
            <div class="col-md-12 profile_fields">
                <p class="p_title">Meta Keywords</p>
                <textarea name="page_meta_keywords" id="" class="form-control" cols="30" rows="10"><?=$page_meta_keywords?></textarea>
            </div>

            <input type="submit" value="حفظ" class="btn btn-info">

        </form>

    </div>

</div>

<script>
    $(function () {

        CKEDITOR.replace( 'page_body' );


    });
</script>

<?php $this->load->view("footer"); ?>

</body>
</html>