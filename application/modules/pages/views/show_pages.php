<?php $this->load->view("header"); ?>


<link rel="stylesheet" href="//cdn.datatables.net/1.10.15/css/jquery.dataTables.min.css">
<script src="//cdn.datatables.net/1.10.15/js/jquery.dataTables.min.js"></script>

<div class="container">
    <div class="col-md-12">
		<div class="main-title">
			<h1>الرئيسية</h1>
		</div>
	</div>

    <div class="col-md-12">

        <?php if(count($pages)): ?>

            <table id="myTable" class="table table-responsive">

                <thead>
                <td>#</td>
                <td>اسم الصفحه</td>
                <td>تعديل</td>
                <td>مسح</td>
                </thead>
                <tbody>
                <?php foreach($pages as $key => $page): ?>
                    <tr id="row_<?= $page->page_id ?>">
                        <td><?= $key + 1; ?></td>
                        <td><?= $page->page_title; ?></td>
                        <td>
                            <a href="<?= site_url("pages/save/$page->page_id") ?>" class="btn btn-info">تعديل</a>
                        </td>
                        <td>
                            <button class="btn btn-danger remove_page" data-id="<?= $page->page_id ?>">مسح</button>
                        </td>
                    </tr>
                <?php endforeach; ?>
                </tbody>
            </table>

        <?php else: ?>
            <div class="alert alert-info">لا توجد صفحات حتي الأن</div>
        <?php endif; ?>

        <div class="col-md-3 col-md-offset-4">
            <a href="<?= site_url("pages/save") ?>" class="btn btn-info">صفحه جديدة</a>
        </div>

    </div>

</div>

<script>
    $(function () {

        $('#myTable').DataTable();


        $('body').on('click', '.remove_page', function () {
            var id = $(this).attr('data-id');
            if(typeof(id) != "undefined" && id > 0)
            {
                var this_element = $(this);
                this_element.attr("disabled","disabled");
                var row_id = $('#row_'+id);
                var url = '<?= site_url('/'); ?>';
                $.ajax({
                    url: url + 'remove_page',
                    type: 'POST',
                    data: {'id':id},
                    success: function (data) {
                        data = JSON.parse(data);
                        if(data.success == "success")
                        {
                            row_id.hide();
                            alert(" تم المسح بنجاح ");
                        }
                        else{
                            this_element.removeAttr("disabled");
                            alert(data.success);
                        }
                    }
                });
            }


        });

    });
</script>

<?php $this->load->view("footer"); ?> 

</body>
</html>